<%-- 
    Document   : search
    Created on : Mar 25, 2016, 5:01:56 PM
    Author     : Al-Amin
--%>
<%@page import="java.sql.DriverManager"%>
<%
//Class.forName("oracle.jdbc.driver.OracleDriver");
  //                  java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "mywebchat", "sust");

 Class.forName("com.mysql.jdbc.Driver");
                    java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mywebchat?zeroDateTimeBehavior=convertToNull", "root", "2011331055");
                    

%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Web Chat</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="homecontents/css/bootstrap.min.css" rel="stylesheet" />
<link href="homecontents/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="homecontents/css/jcarousel.css" rel="stylesheet" />
<link href="homecontents/css/flexslider.css" rel="stylesheet" />
<link href="homecontents/css/style.css" rel="stylesheet" />


<!-- Theme skin -->
<link href="homecontents/skins/default.css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="homepage.jsp"><span>My</span>Web<span>Chat</span></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="homepage.jsp">Home</a></li>
                       
                         <li><a href="search.jsp">Search People</a></li>
                        <%if(session.getAttribute("user_id")==null){%>
                        <li><a href="loginregister.jsp#toregister">Register</a></li>
                        <li><a href="loginregister.jsp#tologin">Login</a></li>
                        <%}else{
                         String user_id =session.getAttribute("user_id").toString(); 
                      Statement st11=con.createStatement();
                     ResultSet rs11=st11.executeQuery("select * from userinfo where user_id like '"+user_id+"'");
                     rs11.next();
                     String profile_url="profile.jsp?profile_id="+user_id;
                     %>
                        <li><a href="<%=profile_url%>">Profile</a></li>
                        <li><a href="homepage.jsp?logout=logout">Logout(<%=rs11.getString("username")%>)</a></li>
            		
                    
                        <%}%>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="homepage.jsp"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Search People</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
				<form id="contactform" action="searchresults.jsp" method="get" >
			                         <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Firstname</h4><input type="text" name="firstname" placeholder="Search by firstname"  />
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                 <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Lastname</h4><input type="text" name="lastname" placeholder="Search by lastname"  />
							<div class="validation">
							</div>
						</div>
                                                                 </div>
					<div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Surname</h4><input type="text" name="surname" placeholder="Search by surname"   />
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                 <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Email</h4><input type="text" name="email" placeholder="Search by email" />
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                 <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Username</h4><input type="text" name="username" placeholder="Search by username"/>
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                 <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by City</h4><input type="text" name="location_city" placeholder="Search by City"/>
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                 <div class="row">
						<div class="col-lg-4 field">
							<h4>Search by Country</h4><input type="text" name="location_country" placeholder="Search by Country"/>
							<div class="validation">
							</div>
						</div>
                                                                 </div>
                                                                  <div class="row">	
						<div class="col-lg-12 margintop10 field">
							<div class="validation">
							</div>
							<p>
								<button class="btn btn-theme margintop10 pull-left" type="submit">Search</button>
								
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</section>
	<footer>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
                                                    <span>&copy; MyWebChat 2016 All right reserved.</span>
                                                    <br> Developed by: Md. Al- Amin & Pranab Singha,
                                                    <br>Department of Computer Science and Engineering,
                                                    <br>Shahjalal University of Science and Technology.
                                                    
						</p>
                        <!-- 
                            All links in the footer should remain intact. 
                            Licenseing information is available at: http://bootstraptaste.com/license/
                            You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Moderna
                        -->
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="https://www.facebook.com" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://www.twitter.com" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://linkedin.com" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="https://google.com/plus" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="homecontents/js/jquery.js"></script>
<script src="homecontents/js/jquery.easing.1.3.js"></script>
<script src="homecontents/js/bootstrap.min.js"></script>
<script src="homecontents/js/jquery.fancybox.pack.js"></script>
<script src="homecontents/js/jquery.fancybox-media.js"></script>
<script src="homecontents/js/google-code-prettify/prettify.js"></script>
<script src="homecontents/js/portfolio/jquery.quicksand.js"></script>
<script src="homecontents/js/portfolio/setting.js"></script>
<script src="homecontents/js/jquery.flexslider.js"></script>
<script src="homecontents/js/animate.js"></script>
<script src="homecontents/js/custom.js"></script>
<script src="homecontents/js/validate.js"></script>
</body>
</html>