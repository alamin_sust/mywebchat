<%@page import="java.util.Random"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Web Chat</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="homecontents/css/bootstrap.min.css" rel="stylesheet" />
<link href="homecontents/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="homecontents/css/jcarousel.css" rel="stylesheet" />
<link href="homecontents/css/flexslider.css" rel="stylesheet" />
<link href="homecontents/css/style.css" rel="stylesheet" />


<!-- Theme skin -->
<link href="homecontents/skins/default.css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
 <%
                   // Class.forName("oracle.jdbc.driver.OracleDriver");
                   // java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "mywebchat", "sust");
                    
                    Class.forName("com.mysql.jdbc.Driver");
                    java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mywebchat?zeroDateTimeBehavior=convertToNull", "root", "2011331055");
                    
                   
                    //ChatBot chatbot=new ChatBot();
                   //out.println(chatbot.get_bot_response("How old are you?"));
                    
                    Random rand = new Random();
                    int room_id=rand.nextInt();
                    if(room_id<0)
                        room_id*=-1;
                    room_id=room_id%5+1;
                    
                    String room_url="chatroom.jsp?room_id="+room_id;
                    
                    if(request.getParameter("logout")!=null&&request.getParameter("logout").toString().startsWith("logout"))
                    {
                        session.setAttribute("user_id", null);
                    }
                    
                    Statement st = con.createStatement();
                    ResultSet rs;
                    Statement st2 = con.createStatement();
                    ResultSet rs2;
                    String user_id=new String();
                    String username=new String();
                    if(session.getAttribute("user_id")!=null)
                    {
                        
                        user_id=session.getAttribute("user_id").toString();
                        st.executeUpdate("update userinfo set now_room='0' where user_id like '"+user_id+"'");
                        
                    }
                    
                    Statement st3 = con.createStatement();
                    ResultSet rs3;
                    if(session.getAttribute("anonymous_id")!=null)
                    {
                        
                    
                        st3.executeUpdate("delete from anonymous where anonymous_id like '"+session.getAttribute("anonymous_id").toString()+"'");
                        session.setAttribute("anonymous_id",null);
                        
                    }
                    
%>

<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="homepage.jsp"><span>My</span>Web<span>Chat</span></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="homepage.jsp">Home</a></li>
                       
                        <li><a href="search.jsp">Search People</a></li>
                        <%if(session.getAttribute("user_id")==null){%>
                        <li><a href="loginregister.jsp#toregister">Register</a></li>
                        <li><a href="loginregister.jsp#tologin">Login</a></li>
                       <%}else{
                     Statement st11=con.createStatement();
                     ResultSet rs11=st11.executeQuery("select * from userinfo where user_id like '"+user_id+"'");
                     rs11.next();
                     String profile_url="profile.jsp?profile_id="+user_id;
                     %>
                        <li><a href="<%=profile_url%>">Profile</a></li>
                        <li><a href="homepage.jsp?logout=logout">Logout(<%=rs11.getString("username")%>)</a></li>
            		
                    
                        <%}%>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="featured">
	<!-- start slider -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
	<!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img src="homecontents/img/slides/1.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Chat Rooms</h3> 
					<p>Chat with thousands of people in Chat Rooms</p> 
					<%if(session.getAttribute("user_id")==null){%><a href="loginregister.jsp" class="btn btn-theme">JOIN NOW</a>
                                        <%}else{%><a href=<%=room_url%> class="btn btn-theme">Enter Now</a><%}%>
                </div>
              </li>
              <li>
                <img src="homecontents/img/slides/2.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Personal Chat</h3> 
					<p>Random personal chat with another user</p> 
					<%if(session.getAttribute("user_id")==null){%><a href="loginregister.jsp" class="btn btn-theme">JOIN NOW</a>
                                                                 <%}else{%><a href="chatindividual.jsp" class="btn btn-theme">Enter Now</a><%}%>
                </div>
              </li>
              <li>
                <img src="homecontents/img/slides/3.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Chat Anonymously</h3> 
					<p>Completely anonymous chat with others</p> 
					<a href="chatanonymous.jsp" class="btn btn-theme">Enter Now</a>
                </div>
              </li>
              <li>
                <img src="homecontents/img/slides/4.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Chat With Bot</h3> 
					<p>Have fun chatting with Our Bot Programs :)</p> 
					<a href="chatbot.jsp" class="btn btn-theme">Enter Now</a>
                </div>
              </li>
            </ul>
        </div>
	<!-- end slider -->
			</div>
		</div>
	</div>	
	
	

	</section>
	<section class="callaction">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="big-cta">
					<div class="cta-text">
						<h2><span>CHAT</span> PEOPLE<span> WORLDWIDE</span> </h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-3">
						<div class="box">
							<div class="box-gray aligncenter">
								<h4>Chat Rooms</h4>
								<div class="icon">
								<i class="fa fa-desktop fa-3x"></i>
								</div>
								<p>
								 Chat with thousands of people in Chat Rooms
								</p>
									
							</div>
							<div class="box-bottom">
								<a href=<%=room_url%> >Enter a Room</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="box">
							<div class="box-gray aligncenter">
								<h4>Personal Chat</h4>
								<div class="icon">
								<i class="fa fa-pagelines fa-3x"></i>
								</div>
								<p>
								 Random personal chat with another user 
                                                                                                           
								</p>
									
							</div>
							<div class="box-bottom">
								<a href="chatindividual.jsp">Start Now</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="box">
							<div class="box-gray aligncenter">
								<h4>Chat Anonymously</h4>
								<div class="icon">
								<i class="fa fa-edit fa-3x"></i>
								</div>
								<p>
								Completely anonymous chat with others 
								</p>
									
							</div>
							<div class="box-bottom">
								<a href="chatanonymous.jsp">Enter Anonymously</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="box">
							<div class="box-gray aligncenter">
								<h4>Chat With Bot</h4>
								<div class="icon">
								<i class="fa fa-code fa-3x"></i>
								</div>
								<p>
                                                                                                        Have fun chatting with Our Bot Programs :)
								</p>
									
							</div>
							<div class="box-bottom">
								<a href="chatbot.jsp">Enter Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="solidline">
				</div>
			</div>
		</div>
                        <h3>Popular Rooms:</h3>
		<!-- end divider -->
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
                                    <%
                                    Statement st12=con.createStatement();
                                    ResultSet rs12=st12.executeQuery("select count(*),now_room from userinfo where now_room like '1' or now_room like '2' or now_room like '3' or now_room like '4' or now_room like '5' group by now_room order by count(*) desc,now_room");
                                    int iter=4; 
                                    while(rs12.next()&& iter>0){
                                    iter--;
                                     String room_url2="chatroom.jsp?room_id="+rs12.getString("now_room");
                                    %>
					<div class="col-lg-3">
						<div class="box">
							<div class="box-gray aligncenter">
								<h4>Chat Room <%=rs12.getString("now_room")%></h4>
								<div class="icon">
								<i class="fa fa-desktop fa-3x"></i>
								</div>
								<p>
								 <%=rs12.getString("count(*)")%> Users Chatting
								</p>
									
							</div>
							<div class="box-bottom">
								<a href=<%=room_url2%> >Enter Room</a>
							</div>
						</div>
					</div>
                                                        <%}%>
				</div>
			</div>
		</div>
		

	</div>
	</section>
	
</div>
                                
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="homecontents/js/jquery.js"></script>
<script src="homecontents/js/jquery.easing.1.3.js"></script>
<script src="homecontents/js/bootstrap.min.js"></script>
<script src="homecontents/js/jquery.fancybox.pack.js"></script>
<script src="homecontents/js/jquery.fancybox-media.js"></script>
<script src="homecontents/js/google-code-prettify/prettify.js"></script>
<script src="homecontents/js/portfolio/jquery.quicksand.js"></script>
<script src="homecontents/js/portfolio/setting.js"></script>
<script src="homecontents/js/jquery.flexslider.js"></script>
<script src="homecontents/js/animate.js"></script>
<script src="homecontents/js/custom.js"></script>
</body>
<div>
<footer>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
                                                    <span>&copy; MyWebChat 2016 All right reserved.</span>
                                                    <br> Developed by: Md. Al- Amin & Pranab Singha,
                                                    <br>Department of Computer Science and Engineering,
                                                    <br>Shahjalal University of Science and Technology.
                                                    
						</p>
                        <!-- 
                            All links in the footer should remain intact. 
                            Licenseing information is available at: http://bootstraptaste.com/license/
                            You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Moderna
                        -->
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="https://www.facebook.com" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://www.twitter.com" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://linkedin.com" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="https://google.com/plus" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer></div>
</html>