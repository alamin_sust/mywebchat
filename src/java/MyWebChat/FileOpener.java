/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWebChat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author User
 */
public class FileOpener {
    public HashMap<String,Integer> mpp=new HashMap<>();
    public ArrayList<String> allKeyStrings=new ArrayList<>();
    public ArrayList<String> allValueStrings=new ArrayList<>();
    public ArrayList<Integer> allValueStringsCounter=new ArrayList<>();
    public void open_and_map_file(String filename) throws FileNotFoundException, IOException{   
       
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
       
        
        
        String line=new String();
        
        
         
        
        ArrayList<String> keyStr= new ArrayList<>();
        ArrayList<String> valStr= new ArrayList<>();
        
        
        
        int counter=0;
        allValueStringsCounter.clear();
        allValueStringsCounter.add(0);
        while((line=bufferedReader.readLine())!=null)
        {
            
               if(line.charAt(0)=='#')
               {
                   for(int i=0;i<keyStr.size();i++)
                   {
                      //if(keyStr.get(i).equals("_I HATE_"))
                      //     System.out.println("ASDFADF:"+valStr.size()+" "+keyStr.size());
                       allValueStringsCounter.add(valStr.size());
                      for(int j=0;j<valStr.size();j++)
                          allValueStrings.add(valStr.get(j));
                   }
                   keyStr.clear();
                   valStr.clear();
               }
               else if(line.charAt(0)=='Q')
               {
                   mpp.put(line.substring(1),++counter);
                   keyStr.add(line.substring(1));
                   allKeyStrings.add(line.substring(1));
               }
               else if(line.charAt(0)=='A')
               {
                   valStr.add(line.substring(1));
               }
               
        }
        
    }
    
    public ArrayList<String> get_all_keys()
    {
        return allKeyStrings;
    }
    
    public ArrayList<String> get_match(String keyStr)
    {
       int indx=mpp.get(keyStr);
       int cum=0;
       for(int i=1;i<indx;i++)
       {
           cum+=allValueStringsCounter.get(i);
       }
       
       ArrayList<String> ret=new ArrayList<>();
       for(int i=cum+1;i<=cum+allValueStringsCounter.get(indx);i++)
           ret.add(allValueStrings.get(i-1));
        return ret;
    }
    
}
