/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWebChat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import javafx.util.Pair;

/**
 *
 * @author User
 */
public class BotResponse {
   String finalOutputString = new String();
    String outputString = new String();
    String outputString2 = new String();
    String contextExtra=new String();
    String preprocessedInputString = new String();
    FileOpener fileOpener = new FileOpener();
    int prevNull=0;
    HashMap<String, ArrayList<String>> prevResponsesMpp = new HashMap<String, ArrayList<String>>();
    HashMap<String, String> mppConv = new HashMap<String, String>();
    public void open_file(String fileName) throws IOException {
        prevResponsesMpp.clear();
        fileOpener.open_and_map_file(fileName);
    }

    public void preprocess_input_string(String inputString) {
        //  System.out.println("<in preprocess_input_string:><"+inputString+">");
        
        mppConv.clear();
        mppConv.put("ME", "YOU");
        mppConv.put("MINE", "YOURS");
        mppConv.put("MYSELF", "YOURSELF");
        mppConv.put("MY", "YOUR");
        mppConv.put("YOU","ME");
        mppConv.put("YOURS","MINE");
        mppConv.put("YOURSELF","MYSELF");
        mppConv.put("YOUR","MY");
        
        inputString = inputString.trim();
        inputString = inputString.toUpperCase();
        preprocessedInputString = "";
        for (int i = 0; i < inputString.length();) {
            if ((inputString.charAt(i)>='A'&&inputString.charAt(i)<='Z')||(inputString.charAt(i)>='0'&&inputString.charAt(i)<='9')) {
                preprocessedInputString += inputString.charAt(i);
                i++;
            } else {
                preprocessedInputString += " ";
                while (i < inputString.length() && (!((inputString.charAt(i)>='A'&&inputString.charAt(i)<='Z')||(inputString.charAt(i)>='0'&&inputString.charAt(i)<='9')))) {
                    i++;
                }
            }
        }
        preprocessedInputString=preprocessedInputString.trim();
        
    }

    public void generate_bot_response(String inputString) throws NullPointerException {
      //  System.out.println("<in generate_bot_response:><"+inputString+">");

        
        int rptcnt=0;
        
        
        String inputWords[] = new String[110];

        ArrayList<String> probableOutputs = new ArrayList<>();
        ArrayList<String> probableOutputs2 = new ArrayList<>();
        ArrayList<String> probableOutputs3=new ArrayList<>();
        ArrayList<String> probableOutputKeys = new ArrayList<>();

        probableOutputKeys = fileOpener.get_all_keys();

        String bestResponse = new String();

        inputWords = inputString.split(" ");

        int distance[] = new int[10010];
        int spellingMistakes[]=new int[10010];
        
        String outputWords[] = new String[110];
        String finalOutputWords[] = new String[110];
        //System.out.print("oPPPPPPPPPP");
        //matching with edit distance
        for (int i = 0; i < probableOutputKeys.size(); i++) {
            
            outputWords = probableOutputKeys.get(i).toString().split(" ");
            //edit distance(Levensthein Distance)
            distance[i] = inputWords.length + outputWords.length;
              spellingMistakes[i]=0;
         //  System.out.println(probableOutputKeys.get(i).toString()+" "+inputWords.length+" "+outputWords.length);
            for (int j = 0, k = 0; j < inputWords.length && k < outputWords.length;) {
                //System.out.println(inputWords[j].length()+"oPPPPPPPPPP"+outputWords[k].length());
                int spellflag=inputWords[j].length()+outputWords[k].length()+1;
                for (int j2 = 0, k2 = 0; j2 < inputWords[j].length() && k2 < outputWords[k].length();) {
                    //System.out.print("o");
                if (inputWords[j].charAt(j2)==outputWords[k].charAt(k2)) {
                    spellflag -= 2;
                    j2++;
                    k2++;
                } else if ((inputWords[j].length()- j2) > (outputWords[k].length() - k2)) {
                    j2++;
                } else if ((inputWords[j].length() - j2) < (outputWords[k].length() - k2)) {
                    k2++;
                } else {
                    j2++;
                    k2++;
                }
                }
                spellflag/=2;
                
                
                if (inputWords[j].equals(outputWords[k])) {
                    distance[i] -= 2;
                    j++;
                    k++;
                }
                else if(spellflag<=1)
                {
                    spellingMistakes[i]+=spellflag;
                    j++;
                    k++;
                }
                else if ((inputWords.length - j) > (outputWords.length - k)) {
                    j++;
                } else if ((inputWords.length - j) < (outputWords.length - k)) {
                    k++;
                } else {
                    j++;
                    k++;
                }
            }

            /*for(int j=0;j<inputWords.length;j++)
             System.out.print(inputWords[j]+"..");
             for(int j=0;j<outputWords.length;j++)
             System.out.print(outputWords[j]+"--");
             System.out.println(distance[i]);*/
        }
        //System.out.print("oPPPPPPPPPP");
        int minDist = 100000010;
        String outputKey = new String();

        for (int i = 0; i < probableOutputKeys.size(); i++) {
            if (minDist > distance[i]||(minDist == distance[i]&&spellingMistakes[i]>0)) {
                outputKey = probableOutputKeys.get(i);
            //probableOutputs.clear();
                probableOutputs.clear();
                probableOutputs = fileOpener.get_match(probableOutputKeys.get(i));
                 finalOutputWords=probableOutputKeys.get(i).split(" ");
                System.out.println("NEW KEY: " + probableOutputKeys.get(i) + " size:" + probableOutputs.size());
                minDist = distance[i];
            }
        }
       
        
        //for template questions
        
        int mxcnt=0;
        if (minDist > 0) {
            //System.out.println("PPP");
            
            for (int i = 0; i < probableOutputKeys.size(); i++) {
                
                outputWords = probableOutputKeys.get(i).toString().split(" ");
            
                int mn=0;
                int mx=outputWords.length-1;
                //System.out.println(probableOutputKeys.get(i).toString()+" "+mn+"PP"+mx);
                 if(outputWords[mn].charAt(0)=='_')
                {
                    if(outputWords[mn].charAt(outputWords[mn].length()-1)=='_')
                        outputWords[mn]=outputWords[mn].substring(0,outputWords[mn].length()-1);
                    outputWords[mn]=outputWords[mn].substring(1,outputWords[mn].length());
                    
                    outputWords[mn]=outputWords[mn].substring(0);
                    int cnt=0, jj=0;
                    for(int j=0,k=0;j<inputWords.length&&k<outputWords.length;jj++,k++,j++)
                    {
                         if(inputWords[j].equals(outputWords[k]))
                             cnt++;
                         
                    }
                    
                   
                    if(cnt==outputWords.length&&cnt>mxcnt){
                        probableOutputs2.clear();
                        probableOutputs2=fileOpener.get_match(probableOutputKeys.get(i));
                    mxcnt=cnt;
                     finalOutputWords=probableOutputKeys.get(i).split(" ");
                   // System.out.println(probableOutputKeys.get(i)+"OOOOOOmn:"+cnt);
                   contextExtra="";
                    for(;jj<inputWords.length;jj++)
                    {
                        if(mppConv.containsKey(inputWords[jj].trim()))
                            contextExtra=mppConv.get(inputWords[jj].trim())+contextExtra;
                        else
                        contextExtra=inputWords[jj]+contextExtra;
                        contextExtra=" "+contextExtra;
                    }
                    contextExtra=contextExtra.trim();
                    }
                }
                else if(outputWords[mx].charAt(outputWords[mx].length()-1)=='_')
                {
                    outputWords[mx]=outputWords[mx].substring(0,outputWords[mx].length()-1);
                     int cnt=0,jj=inputWords.length-1;
                    for(int j=inputWords.length-1,k=outputWords.length-1;j>=0&&k>=0;jj--,k--,j--)
                    {
                         if(inputWords[j].equals(outputWords[k]))
                             cnt++;
                    }
                    if(cnt==outputWords.length&&cnt>mxcnt){
                        probableOutputs2.clear();
                        probableOutputs2=fileOpener.get_match(probableOutputKeys.get(i));
                         finalOutputWords=probableOutputKeys.get(i).split(" ");
                      //  System.out.println(probableOutputKeys.get(i)+"OOOOOOmx:"+cnt);
                    mxcnt=cnt;
                    contextExtra="";
                    for(;jj>=0;jj--)
                    {
                        if(mppConv.containsKey(inputWords[jj].trim()))
                            contextExtra=mppConv.get(inputWords[jj].trim())+contextExtra;
                        else
                        contextExtra=inputWords[jj]+contextExtra;
                        contextExtra=" "+contextExtra;
                    }
                    contextExtra=contextExtra.trim();
                    }
                }
                        else if(outputWords[mn].charAt(0)=='_'&&outputWords[mx].charAt(outputWords[mx].length()-1)=='_')
                {
                    outputWords[mx]=outputWords[mx].substring(0,outputWords[mx].length()-1);
                    outputWords[mn]=outputWords[mn].substring(1,outputWords[mn].length());
                    System.out.println(mn+" "+mx+" "+outputWords[mn]);
                    int cnt=0;
                    for(int p=0;p<inputWords.length;p++)
                    {
                        cnt=0;
                         for(int j=p,k=0;j<inputWords.length&&k<outputWords.length;j++,k++)
                         {
                             if(inputWords[j].equals(outputWords[k]))
                                 cnt++;
                         }
                         if(cnt==outputWords.length)
                             break;
                    }
                    if(cnt==outputWords.length&&cnt>mxcnt)
                    {
                        probableOutputs2.clear();
                        probableOutputs2=fileOpener.get_match(probableOutputKeys.get(i));
                         finalOutputWords=probableOutputKeys.get(i).split(" ");
                        //System.out.println(probableOutputKeys.get(i)+"OOOOOOmnmx:"+cnt+" sz:"+probableOutputs.size());
                        mxcnt=cnt;
                    }
                    for(int j=0;j<inputWords.length;j++)
                    {
                   //     System.out.println("I: "+inputWords[j]);
                    }
                    for(int k=0;k<outputWords.length;k++)
                    {
                     //   System.out.println("O: "+outputWords[k]);
                    }
                    // System.out.println("cnt: "+cnt);
                }
            }
        }

        for (int i = 0; i < probableOutputs.size(); i++) {
            System.out.println("BOT probalble outputs(" + i + "): " + probableOutputs.get(i) + " Dist: " + minDist);
        }
        for (int i = 0; i < probableOutputs2.size(); i++) {
            System.out.println("BOT probalble outputs2(" + i + "): " + probableOutputs2.get(i) + " Similarity: " + mxcnt);
        }

        Random rand = new Random();
        int indx;
        indx = rand.nextInt();
        if (indx < 0) {
            indx *= -1;
        }
        indx %= probableOutputs.size();
        System.out.println(indx);
        ArrayList<String> tmpStr = new ArrayList<>();

        if (prevResponsesMpp.containsKey(outputKey)) {
            tmpStr = prevResponsesMpp.get(outputKey);
        }

        /*for(int i=0;i<probableOutputs.size();i++)
         {
         if(i==indx)
         {
         tmpStr.add(probableOutputs.get(i));
         }
         }*/
        if (probableOutputs.size() > indx) {
            tmpStr.add(probableOutputs.get(indx));
        }

        if (tmpStr.size() > 0) {
            prevResponsesMpp.put(outputKey, tmpStr);
        }

        if (probableOutputs.size() > indx && prevResponsesMpp.get(outputKey).size() <= 3) {
            outputString = probableOutputs.get(indx);
            
        } else {
            //rptcnt++;
           // rptcnt%=3;
            //outputString = repeatingMsg[rptcnt];
            probableOutputs.clear();
            probableOutputs=fileOpener.get_match(probableOutputKeys.get(0));
             finalOutputWords=probableOutputKeys.get(0).split(" ");
            indx=rand.nextInt();
            if(indx<0)
                indx*=-1;
            indx%=probableOutputs.size();
            outputString=probableOutputs.get(indx);
        }
        if(!probableOutputs2.isEmpty())
        {indx=rand.nextInt();
            if(indx<0)
                indx*=-1;
            indx%=probableOutputs2.size();
            outputString2=probableOutputs2.get(indx);
           for(int i=0;i<outputString2.length();i++)
           {
               if(outputString2.charAt(i)=='*')
               {
                   outputString2=outputString2.substring(0,i)+contextExtra+outputString2.substring(i+1);
                   break;
               }
           }
           System.out.println(indx+" :OOO"+outputString2);
        }
           if((inputWords.length+outputWords.length-minDist)/2<=mxcnt&&outputString2.length()>0)
           {
               System.out.println("S");
               finalOutputString=outputString2;
           }
           else
           {
               System.out.println("T");
               finalOutputString=outputString;
           }
           
           System.out.println(inputWords.length+"HH");
           
           if(inputWords.length==0||(inputWords.length==1&&inputWords[0].trim().length()==0))
           {
               
               if(prevNull==1)
               {
                   probableOutputs3.clear();
               probableOutputs3=fileOpener.get_match(probableOutputKeys.get(3));
                finalOutputWords=probableOutputKeys.get(3).split(" ");
               indx=rand.nextInt();
               if(indx<0)
                   indx*=-1;
               indx%=probableOutputs3.size();
               System.out.println("U");
               finalOutputString=probableOutputs3.get(indx);
                   
               }
               else
               {
                   probableOutputs3.clear();
               probableOutputs3=fileOpener.get_match(probableOutputKeys.get(2));
                finalOutputWords=probableOutputKeys.get(2).split(" ");
               indx=rand.nextInt();
               if(indx<0)
                   indx*=-1;
               indx%=probableOutputs3.size();
               System.out.println("V");
               finalOutputString=probableOutputs3.get(indx);
               }
               for(int i=0;i<probableOutputs3.size();i++)
               {
                   System.out.println("BOT probalble outputs3(" + i + "): " + probableOutputs3.get(i) + " PREVNULL="+prevNull);
               }
               prevNull=1;
               
           }
           else prevNull=0;
           System.out.println("totlen: "+finalOutputWords.length);
           if((inputWords.length+finalOutputWords.length)==minDist&&mxcnt==0)
           {
               probableOutputs3.clear();
               probableOutputs3=fileOpener.get_match(probableOutputKeys.get(4));
               indx=rand.nextInt();
                finalOutputWords=probableOutputKeys.get(4).split(" ");
               if(indx<0)
                   indx*=-1;
               indx%=probableOutputs3.size();
               System.out.println("X");
               finalOutputString=probableOutputs3.get(indx);
               for(int i=0;i<probableOutputs3.size();i++)
               {
               System.out.println("BOT probalble outputs3(" + i + "): " + probableOutputs3.get(i) + " NoMatch");
           
               }
           }
    }
    public String get_welcome_msg(){
        ArrayList<String> probableOutputs = new ArrayList<>();
       probableOutputs=fileOpener.get_match("SIGNIN**");
       Random rand=new Random();
       int indx=0;
       indx=rand.nextInt();
               if(indx<0)
                   indx*=-1;
               indx%=probableOutputs.size();
               System.out.println("Y");
               finalOutputString=probableOutputs.get(indx);
               return finalOutputString;
    }
    public String get_preprocessed_string() {
        return preprocessedInputString;
    }

    public String get_output_string() {
        return finalOutputString;
    }

}
