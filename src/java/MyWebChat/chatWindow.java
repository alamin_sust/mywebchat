/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWebChat;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author amansehgal
 */
@WebServlet(name = "chatWindow", urlPatterns = {"/chatWindow"})
public class chatWindow extends HttpServlet {

    String username, tempName;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String message = request.getParameter("txtMsg");  //Extract Message
            String username = session.getAttribute("username").toString(); //Extract Username

            //Display Chat Room
            out.println(" <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>");
            out.println("<script type=\"text/javascript\">");
            out.println("var url = 'chatWindow'");
            out.println("$(document).ready(function() {");
            out.println("$.ajaxSetup({ cache: false });");
            out.println("setInterval(function() {$(\"#score\").load(url); }, 10000);");
            out.println("});");
            out.println("</script>");
            String header=new String();
            if(request.getHeader("Referer")==null)
                {
                    header="http://localhost:8084/MyWebChat/chatWindow";
                }
                else
                {
                    header=request.getHeader("Referer");
                }
            if(header.startsWith("http://localhost:8084/MyWebChat/userLogin")||request.getParameter("txtMsg") != null)
            {
            out.println("<html>  <head><body bgcolor=\"#6495ED\"> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <title>Chat Room</title>  </head>");
            out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <center>");
            out.println("<h2>Hi ");
            out.println(username);
            out.println("<br> Welcome to Hello Chat ");
            out.println("</h2><br><hr>");
            out.println("  <body>");
            out.println("      <form name=\"chatWindow\" action=\"chatWindow\">");
            out.println("Message: <input type=\"text\" name=\"txtMsg\" value=\"\" /><input type=\"submit\" value=\"Send\" name=\"cmdSend\"/>");
            out.println("<br><br> <a href=\"chatWindow\">Refresh Chat Room</a>");
            out.println("<br>  <br>");}

//            out.println("<script type=\"text/javascript\" src=\"code.jquery.com/jquery-1.4.2.min.js\"></script>");
//           out.println("<script src=\"//code.jquery.com/jquery-1.12.0.min.js\"></script>");
//           out.println("<script src=\"//code.jquery.com/jquery-migrate-1.2.1.min.js\"></script>");
//            out.println("<script type=\"text/javascript\">");
//            out.println("var auto = setInterval(    function ()");
//            out.println("{");
//            out.println("$('#score').load('chatWindow.java').fadeIn(\"slow\");");
//            out.println("}, 5000); // refresh every 5000 milliseconds");
//            out.println("</script>");
            
            

            out.println(" <div id=\"score\"> Messages in Chat Box:");
            out.println("<br><br>");
            out.println("<textarea  readonly=\"readonly\"   name=\"txtMessage\" rows=\"20\" cols=\"60\">");

            //If eneterd message != null then insert into database
            if (request.getParameter("txtMsg") != null) {
                //response.setIntHeader("Refresh", 5);
                try {
                    Class.forName("oracle.jdbc.driver.OracleDriver");
                    java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "mywebchat", "sust");

                    Statement st = con.createStatement();

                    String sql = "insert into conversation values ('" + username + "','" + message + "')";
                    st.executeUpdate(sql);

                    st.execute("commit");
                   

                } catch (Exception ex1) {
                    System.err.println(ex1.getMessage());
                    String messages = "No";
                    //out.println(messages);
                }
            }
            // Retrieve all messages from database

            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "mywebchat", "sust");

                Statement st = con.createStatement();

                ResultSet rs = st.executeQuery("select * from conversation");

                // Print all retrieved messages
                while (rs.next()) {
                    String messages = rs.getString("username") + " >> " + rs.getString("message");

                    out.println(messages);
                }

                con.close();
            } catch (Exception ex1) {
                System.err.println(ex1.getMessage());
            }

            out.println("</textarea></div>");
            out.println("<hr>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Session 

        session = request.getSession();

        if (username != null) {
            tempName = username;
        }

        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    HttpSession session;
}
