<%-- 
    Document   : loginregister
    Created on : Mar 24, 2016, 7:10:23 PM
    Author     : User
--%>






<%@page import="java.sql.DriverManager"%>
<%
                    //Class.forName("oracle.jdbc.driver.OracleDriver");
                    //java.sql.Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "mywebchat", "sust");
                    
                     Class.forName("com.mysql.jdbc.Driver");
                    java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mywebchat?zeroDateTimeBehavior=convertToNull", "root", "2011331055");
                    
                    
                    if(session.getAttribute("user_id")!=null)
                    {                     
                        response.sendRedirect("homepage.jsp");
                    }
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>Login and Registration Form with HTML5 and CSS3</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
    </head>
    <body>
        <div class="container">
            <header>
              
            </header>
            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            
                            <form  action="success.jsp" autocomplete="on"> 
                                <h1>Log in</h1> 
                                
                                <p> 
                                    <label for="username" class="uname" data-icon="u" > Your username </label>
                                    <input id="username" name="username" required="required" type="text" placeholder="myusername or mymail@mail.com"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                    <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                </p>
                                <p class="keeplogin"> 
									<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
									<label for="loginkeeping">Keep me logged in</label>
								</p>
                                <p class="login button">
                                    <%String header=new String();
                    if(request.getHeader("Referer")==null)
                {
                    header="homepage.jsp";
                }
                else
                {
                    header=request.getHeader("Referer");
                }
                                    if(header.endsWith("loginregister.jsp")){
                                    %>
                                    Please Try Again!
                                    <%}%>
                                    <input type="submit" value="Login" /> 
								</p>
                                <p class="change_link">
									Not a member yet ?
									<a href="#toregister" class="to_register">Join us</a>
								</p>
                            </form>
                            
                        </div>

                        <div id="register" class="animate form">
                            <form  action="success.jsp" autocomplete="on"> 
                                <h1> Sign up </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Your firstname</label>
                                    <input id="usernamesignup" name="firstname" required="required" type="text" placeholder="Md. " />
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Your lastname</label>
                                    <input id="usernamesignup" name="lastname" required="required" type="text" placeholder="Al-Amin" />
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Your surname</label>
                                    <input id="usernamesignup" name="surname" required="required" type="text" placeholder="Nitol" />
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>
                                    <input id="emailsignup" name="email" required="required" type="email" placeholder="alaminbbsc@gmail.com"/> 
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Your username</label>
                                    <input id="usernamesignup" name="username" required="required" type="text" placeholder="alamin_sust" />
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">Your password </label>
                                    <input id="passwordsignup" name="password" required="required" type="password" placeholder="eg. 1234WXYZ"  data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                    <input id="passwordsignup_confirm" name="password2" required="required" type="password" placeholder="eg. 1234WXYZ"  data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Your City</label>
                                    <input id="usernamesignup" name="location_city" required="required" type="text" placeholder="Sylhet" />
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">You Country</label>
                                    <input id="usernamesignup" name="location_country" required="required" type="text" placeholder="Bangladesh" />
                                </p>
                                <p> 
                                    <label for="usernamesignup" class="uname" data-icon="u">Profile Message</label>
                                    <input id="usernamesignup" name="profile_message" required="required" type="text" placeholder="Feeling Cool :)" />
                                </p>
                                
                                
                                
                                
                                <p class="signin button"> 
                                      <%
                    if(request.getHeader("Referer")==null)
                {
                    header="homepage.jsp";
                }
                else
                {
                    header=request.getHeader("Referer");
                }
                                    if(header.endsWith("loginregister.jsp")){
                                    %>
                                    Please Try Again!
                                    <%}%>
									<input type="submit" value="Sign up"/> 
								</p>
                                <p class="change_link">  
									Already a member ?
									<a href="#tologin" class="to_register"> Go and log in </a>
								</p>
                            </form>
                        </div>
						
                    </div>
                </div>  
            </section>
        </div>
    </body>
</html>